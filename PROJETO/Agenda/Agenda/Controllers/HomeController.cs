﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Agenda.Models;


namespace Agenda.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Listar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult _Listar(string agendaListar)
        {
            // chamo  a conexao 
            ClassConexao.ConexaoDB conexao = new ClassConexao.ConexaoDB();
            SqlConnection conn = conexao.Conectar();
            DataTable dataTable = new DataTable();

            // instancio o objeto para desserializar o json que veio via ajax da view
            ListaAgenda agenda = new ListaAgenda();

            // recebo a informação do parametro e uso como entrada no parametro da procedure
            agenda = JsonConvert.DeserializeObject<ListaAgenda>(agendaListar);

            // chamada da procedure passando os parametros
            SqlCommand Command = new SqlCommand("[dbo].[sp_Agenda_filtrar]", conn);
            Command.CommandType = CommandType.StoredProcedure;
            Command.Parameters.AddWithValue("@evento", agenda.evento);

            // preencho data table e retorno a partialview os dados dele
            using (SqlDataAdapter da = new SqlDataAdapter(Command))
            {
                da.Fill(dataTable);
            }

            var result = Command.ExecuteNonQuery();
            conn.Close();

            //essa sessão eu utilizo na partial view para iterar e montar a tabela com essas informações
            Session["AgendaLista"] = dataTable;
          
            return PartialView(dataTable);
        }

        [HttpGet]
        public ActionResult Cadastrar()
        {

            ClassConexao.ConexaoDB conexao = new ClassConexao.ConexaoDB();
            SqlConnection conn = conexao.Conectar();
            DataTable dataTable = new DataTable();
            // chamada da procedure passando os parametros
            SqlCommand Command = new SqlCommand("[dbo].[sp_PopularCategoriaCombo]", conn);
            Command.CommandType = CommandType.StoredProcedure;

            // preencho data table e retorno a partialview os dados dele
            using (SqlDataAdapter da = new SqlDataAdapter(Command))
            {
                da.Fill(dataTable);
            }

            var result = Command.ExecuteNonQuery();
            conn.Close();

            //essa sessão eu utilizo na partial view para iterar e montar a tabela com essas informações
            ViewBag.Combo = dataTable;

            return View();
           
        }

        [HttpPost]
        public ActionResult Cadastrar(ListaAgenda listaAgenda)
        {
            ClassConexao.ConexaoDB conexao = new ClassConexao.ConexaoDB();
            SqlConnection conn = conexao.Conectar();

            // chamada da procedure passando os parametros
            SqlCommand Command = new SqlCommand("[dbo].[sp_Agenda_Inserir]", conn);
            Command.CommandType = CommandType.StoredProcedure;
            Command.Parameters.AddWithValue("@agendaTitulo", listaAgenda.agendaTitulo);
            Command.Parameters.AddWithValue("@agendaDescricao", listaAgenda.agendaDescricao);
            Command.Parameters.AddWithValue("@agendaData", Convert.ToDateTime(listaAgenda.agendaData));
            Command.Parameters.AddWithValue("@agendaCategoria", listaAgenda.categoria);

            var param = Command.Parameters.Add("@saida", SqlDbType.VarChar, 300);
            param.Direction = ParameterDirection.Output;

            var inserido = Command.ExecuteNonQuery();

            string result = param.Value.ToString().Trim();
            return RedirectToAction("Listar", "Home");
        }

        public string Deletar(int Id)
        {
            // chamada da conexao
            ClassConexao.ConexaoDB conexao = new ClassConexao.ConexaoDB();
            SqlConnection conn = conexao.Conectar();

            // passando parametro para a procedure
            SqlCommand Command = new SqlCommand("[dbo].[sp_Agenda_Excluir]", conn);
            Command.CommandType = CommandType.StoredProcedure;
            Command.Parameters.AddWithValue("@agendaId", Id);

            // retorno de saida 
            var param = Command.Parameters.Add("@saida", SqlDbType.VarChar, 300);
            param.Direction = ParameterDirection.Output;

            var deletado = Command.ExecuteNonQuery();

            string result = param.Value.ToString().Trim();

            conn.Close();

            // RETORNO UMA STRING DE SAIDA EM CASO DE ERRO OU ID DA NOTA EM CASO DE SUCESSO
            return result;
        }
    }
}