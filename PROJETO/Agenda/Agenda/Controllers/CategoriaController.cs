﻿using Agenda.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Agenda.Controllers
{
    public class CategoriaController : Controller
    {
        //
        // GET: /Categoria/
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Inserir()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Inserir(Categoria categoria)
        {
            ClassConexao.ConexaoDB conexao = new ClassConexao.ConexaoDB();
            SqlConnection conn = conexao.Conectar();

            // chamada da procedure passando os parametros
            SqlCommand Command = new SqlCommand("[dbo].[sp_Categoria_Inserir]", conn);
            Command.CommandType = CommandType.StoredProcedure;
            Command.Parameters.AddWithValue("@CategoriaDescricao", categoria.categoriaDescricao);
            Command.Parameters.AddWithValue("@categoriaDtCadastro", categoria.categoriaDtCadastro);

            var param = Command.Parameters.Add("@saida", SqlDbType.VarChar, 300);
            param.Direction = ParameterDirection.Output;

            var inserido = Command.ExecuteNonQuery();

            string result = param.Value.ToString().Trim();
            // return RedirectToAction("Index", "Home");

            ViewBag.Message = result;
            return View();

        }

        public ActionResult _Buscar()
        {
            // chamo  a conexao 
            ClassConexao.ConexaoDB conexao = new ClassConexao.ConexaoDB();
            SqlConnection conn = conexao.Conectar();
            DataTable dataTable = new DataTable();

            // chamada da procedure passando os parametros
            SqlCommand Command = new SqlCommand("[dbo].[sp_Categoria_buscar]", conn);
            Command.CommandType = CommandType.StoredProcedure;

            var param = Command.Parameters.Add("@saida", SqlDbType.VarChar, 300);
            param.Direction = ParameterDirection.Output;

            // preencho data table e retorno a partialview os dados dele
            using (SqlDataAdapter da = new SqlDataAdapter(Command))
            {
                da.Fill(dataTable);
            }

            var result = Command.ExecuteNonQuery();
            conn.Close();

            //essa sessão eu utilizo na partial view para iterar e montar a tabela com essas informações
            Session["CategoriaLista"] = dataTable;
            return PartialView(dataTable);
        }
    }
}