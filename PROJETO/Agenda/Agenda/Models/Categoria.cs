﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agenda.Models
{
    public class Categoria
    {
        public int categoriaID { get; set; }
        public string categoriaDescricao { get; set; }
        public DateTime categoriaDtCadastro { get; set; }
    }
}