﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Agenda.Models
{
    public class ListaAgenda
    {
        public int agendaId { get; set; }
        public string agendaTitulo { get; set; }
        public string agendaDescricao { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public string agendaData { get; set; }
        public string evento { get; set; }
        public string categoria { get; set; }
    }
}