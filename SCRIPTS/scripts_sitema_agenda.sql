
CREATE DATABASE AgendaDB

USE [AgendaDB]
GO
/****** Object:  Table [dbo].[tb_Agenda]    Script Date: 10/11/2017 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Agenda](
	[agendaID] [int] IDENTITY(1,1) NOT NULL,
	[agendaTitulo] [varchar](30) NOT NULL,
	[agendaDescricao] [varchar](100) NOT NULL,
	[agendaData] [datetime] NOT NULL,
	[categoriaID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[agendaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tb_Categoria]    Script Date: 10/11/2017 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tb_Categoria](
	[categoriaID] [int] IDENTITY(1,1) NOT NULL,
	[categoriaDescricao] [varchar](50) NOT NULL,
	[categoriaDtCadastro] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[categoriaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tb_Agenda]  WITH CHECK ADD  CONSTRAINT [fk_agenda_categoria] FOREIGN KEY([categoriaID])
REFERENCES [dbo].[tb_Categoria] ([categoriaID])
GO
ALTER TABLE [dbo].[tb_Agenda] CHECK CONSTRAINT [fk_agenda_categoria]
GO
/****** Object:  StoredProcedure [dbo].[sp_Agenda_Excluir]    Script Date: 10/11/2017 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_Agenda_Excluir](
@agendaId int,
@saida varchar(100) output) as
	begin
		if exists(select agendaId from tb_Agenda where agendaId = @agendaId)
			begin
				delete from tb_Agenda where agendaId = @agendaId
				set @saida = 'SUCESSO: AGENDAMENTO NUMERO ' + CONVERT(VARCHAR,@agendaId) + ' DELETADO COM SUCESSO!'
			end
		else
			set @saida = 'ERRO: AGENDAMENTO NUMERO ' + CONVERT(VARCHAR,@agendaId) + ' NAO ENCONTRADO NA BASE!'
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_Agenda_filtrar]    Script Date: 10/11/2017 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_Agenda_filtrar](
@evento varchar(20)) as
	begin
		if @evento = 'HOJE'
			select A.agendaID,A.agendaDescricao, A.agendaTitulo, A.agendaData,
			C.categoriaDescricao from tb_Agenda A 
			inner join tb_Categoria C ON(A.categoriaID = C.categoriaID)
			 where A.agendaData >= convert(date,getdate())
			and A.agendaData <= getdate()
			order by agendaData desc
		else if @evento = 'PASSADO'
			select A.agendaID,A.agendaDescricao, A.agendaTitulo, A.agendaData,
			C.categoriaDescricao from tb_Agenda A 
			inner join tb_Categoria C ON(A.categoriaID = C.categoriaID)
			where A.agendaData <= convert(date,getdate())
			order by agendaData desc
		else if @evento = 'FUTURO'
			select A.agendaID,A.agendaDescricao, A.agendaTitulo, A.agendaData,
			C.categoriaDescricao from tb_Agenda A 
			inner join tb_Categoria C ON(A.categoriaID = C.categoriaID) 
			where A.agendaData >= dateadd(day,1,convert(date,getdate()))
			order by agendaData desc
		else if @evento = 'SELECIONE'
			select A.agendaID,A.agendaDescricao, A.agendaTitulo, A.agendaData,
			C.categoriaDescricao from tb_Agenda A 
			inner join tb_Categoria C ON(A.categoriaID = C.categoriaID)
			order by agendaData desc
	end	

GO
/****** Object:  StoredProcedure [dbo].[sp_Agenda_Inserir]    Script Date: 10/11/2017 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_Agenda_Inserir](
@agendaTitulo varchar(30),
@agendaDescricao varchar(100),
@agendaData datetime,
@agendaCategoria int,
@saida varchar(100) output) as

	begin
		insert into tb_Agenda (agendaTitulo,agendaDescricao, agendaData, categoriaID)
					values(@agendaTitulo, @agendaDescricao, @agendaData, @agendaCategoria)
		if(@@IDENTITY > 0)
			set @saida = 'SUCESSO: AGENDAMENTO NUMERO ' + CONVERT(VARCHAR,@@IDENTITY) + ' INSERIDO COM SUCESSO!'
			
		else
			set @saida = 'ERRO: REGISTRO NAO INSERIDO!'
			
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_Categoria_buscar]    Script Date: 10/11/2017 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_Categoria_buscar] (@saida varchar(50) output)
as
SET NOCOUNT ON
begin
	select categoriaID, categoriaDescricao, categoriaDtCadastro from tb_Categoria
	if(@@ROWCOUNT <= 0)
		set @saida = 'NENHUM REGISTRO ENCONTRADO!'
end
GO
/****** Object:  StoredProcedure [dbo].[sp_Categoria_Inserir]    Script Date: 10/11/2017 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_Categoria_Inserir](
@CategoriaDescricao varchar(50),
@categoriaDtCadastro datetime,
@saida varchar(100) output) as

	begin
		insert into tb_Categoria (categoriaDescricao, categoriaDtCadastro)
					values(@CategoriaDescricao, @categoriaDtCadastro)
		if(@@IDENTITY > 0)
			set @saida = 'SUCESSO: CATEGORIA ' + CONVERT(VARCHAR,@CategoriaDescricao) + ' INSERIDA COM SUCESSO!'
			
		else
			set @saida = 'ERRO: CATEGORIA NAO INSERIDA!'
			
	end

GO
/****** Object:  StoredProcedure [dbo].[sp_PopularCategoriaCombo]    Script Date: 10/11/2017 17:12:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[sp_PopularCategoriaCombo] as
begin
	select C.categoriaID, C.categoriaDescricao
	from tb_Categoria C
	
end

GO
